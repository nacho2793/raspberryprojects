from django.conf.urls import patterns, include, url
from django.contrib import admin
from houseCtrllr import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'intHouse.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url(r'^ctrlSwtch/(?P<pin>[0-9]{2})/(?P<state>[0-9]{1})/$', views.ctrlSwtch, name = 'ctrlSwtch'),
    url(r'^pwm/(?P<pinW>[0-9]{2})/(?P<frecuency>[0-9]{2})/(?P<onTime>[0-9]{2})/$', views.pwm, name = 'pwm'),
)
