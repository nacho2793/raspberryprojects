from django.shortcuts import render
import time
from django.http import HttpResponse
import RPi.GPIO as GPIO

# Create your views here.
def ctrlSwtch(request,state, pin):
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(int(pin),GPIO.OUT)
	if(state=='1'):
		GPIO.output(int(pin),GPIO.HIGH)
	elif(state=='0'):
		GPIO.output(int(pin),GPIO.LOW)
	answer = 'Pin %s : %s' %(pin, state)
	return HttpResponse(answer)
def pwm(request, pinW, onTime, frecuency):
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(int(pinW), GPIO.OUT)
	answer = 'PWM with %s frecuency and %s dutyCycle in pin %s' %(frecuency, onTime, pinW)
	motor = GPIO.PWM(int(pinW), float(frecuency))
	motor.start(float(onTime))
	time.sleep(5)
	motor.stop()
	GPIO.cleanup()
	
	return HttpResponse(answer)
